﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services.Abstractions
{
    public interface IPartnersService
    {
        Task<List<PartnerResponse>> GetPartnersAsync();

        Task<PartnerPromoCodeLimitResponse> GetPartnerLimitAsync(Guid id, Guid limitId);

        Task<(Guid partnerId, Guid newLimitId)> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request);
        
        Task CancelPartnerPromoCodeLimitAsync(Guid id);
    }
}
