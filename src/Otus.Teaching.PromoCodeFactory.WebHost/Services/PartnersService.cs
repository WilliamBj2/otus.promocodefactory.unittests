﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.CustomExtensions.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class PartnersService : IPartnersService
    {
        private readonly IRepository<Partner> _partnersRepository;

        public PartnersService(IRepository<Partner> partnersRepository)
        {
            _partnersRepository = partnersRepository;
        }

        public async Task<List<PartnerResponse>> GetPartnersAsync()
        {
            var partners = await _partnersRepository.GetAllAsync();

            return partners.Select(x => new PartnerResponse()
            {
                Id = x.Id,
                Name = x.Name,
                NumberIssuedPromoCodes = x.NumberIssuedPromoCodes,
                IsActive = true,
                PartnerLimits = x.PartnerLimits
                    .Select(y => new PartnerPromoCodeLimitResponse()
                    {
                        Id = y.Id,
                        PartnerId = y.PartnerId,
                        Limit = y.Limit,
                        CreateDate = y.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        EndDate = y.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        CancelDate = y.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
                    })
                    .ToList()
            })
            .ToList();
        }

        public async Task<PartnerPromoCodeLimitResponse> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            var partner = await _partnersRepository.GetByIdAsync(id)
                ?? throw new NotFoundException();

            var limit = partner.PartnerLimits.FirstOrDefault(x => x.Id == limitId);
            return limit is null
                ? null
                : new PartnerPromoCodeLimitResponse()
                {
                    Id = limit.Id,
                    PartnerId = limit.PartnerId,
                    Limit = limit.Limit,
                    CreateDate = limit.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                    EndDate = limit.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                    CancelDate = limit.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
                };
        }

        public async Task<(Guid partnerId, Guid newLimitId)> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request)
        {
            var partner = await _partnersRepository.GetByIdAsync(id)
                ?? throw new NotFoundException();

            // Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                throw new ConflictException("Данный партнер не активен");

            // Установка лимита партнеру
            if (partner.PartnerLimits.FirstOrDefault(x =>
                    !x.CancelDate.HasValue) is PartnerPromoCodeLimit activeLimit)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;

                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = DateTime.Now;
            }

            if (request.Limit <= 0)
                throw new ConflictException("Лимит должен быть больше 0");

            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = request.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = request.EndDate
            };

            partner.PartnerLimits.Add(newLimit);
            await _partnersRepository.UpdateAsync(partner);

            return (partner.Id, newLimit.Id);
        }

        public async Task CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            var partner = await _partnersRepository.GetByIdAsync(id)
                ?? throw new NotFoundException();

            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                throw new ConflictException("Данный партнер не активен");

            //Отключение лимита
            var activeLimit = partner.PartnerLimits
                .FirstOrDefault(x => !x.CancelDate.HasValue);

            if (activeLimit != null)
            {
                activeLimit.CancelDate = DateTime.Now;
                await _partnersRepository.UpdateAsync(partner);
            }
        }
    }
}
