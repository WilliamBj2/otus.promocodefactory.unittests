﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.CustomExtensions.Exceptions
{
    public class ConflictException : Exception
    {
        public ConflictException() : base(string.Empty) { }

        public ConflictException(string message) : base(message) { }
    }
}
