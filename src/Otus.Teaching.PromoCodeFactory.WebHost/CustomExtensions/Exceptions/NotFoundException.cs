﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.CustomExtensions.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException() : base("Данные не обнаружены") { }

        public NotFoundException(string message) : base(message) { }
    }
}
