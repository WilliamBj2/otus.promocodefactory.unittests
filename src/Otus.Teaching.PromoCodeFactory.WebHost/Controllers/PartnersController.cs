﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.CustomExtensions.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Партнеры
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PartnersController
        : ControllerBase
    {
        private readonly IPartnersService _partnersService;

        public PartnersController(IPartnersService partnersService)
        {
            _partnersService = partnersService;
        }

        [HttpGet]
        public Task<ActionResult<List<PartnerResponse>>> GetPartnersAsync()
        {
            return ExecuteAsync<List<PartnerResponse>>(async () =>
            {
                var result = await _partnersService.GetPartnersAsync();
                return Ok(result);
            });
        }

        [HttpGet("{id}/limits/{limitId}")]
        public Task<ActionResult<PartnerPromoCodeLimit>> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            return ExecuteAsync<PartnerPromoCodeLimit>(async () =>
            {
                var result = await _partnersService.GetPartnerLimitAsync(id, limitId);
                return Ok(result);
            });
        }

        [HttpPost("{id}/limits")]
        public Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request)
        {
            return ExecuteAsync(async () =>
            {
                (Guid partnerId, Guid newLimitId) = await _partnersService.SetPartnerPromoCodeLimitAsync(id, request);
                return CreatedAtAction(
                    nameof(GetPartnerLimitAsync),
                    new { id = partnerId, limitId = newLimitId },
                    null);
            });
        }

        [HttpPost("{id}/canceledLimits")]
        public Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            return ExecuteAsync(async () =>
            {
                await _partnersService.CancelPartnerPromoCodeLimitAsync(id);
                return NoContent();
            });
        }

        private async Task<IActionResult> ExecuteAsync(Func<Task<IActionResult>> func)
        {
            try
            {
                return await func.Invoke();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        private async Task<ActionResult<T>> ExecuteAsync<T>(Func<Task<ActionResult<T>>> func)
        {
            try
            {
                return await func.Invoke();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}