﻿using AutoFixture;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders
{
    public class PartnersBuilder
    {
        private AutoFixture.Dsl.IPostprocessComposer<Partner> _fixture;

        public PartnersBuilder()
        {
            _fixture = new Fixture()
                .Build<Partner>()
                .Without(x => x.PartnerLimits);
        }

        public PartnersBuilder WithId(Guid value)
        {
            _fixture = _fixture.With(x => x.Id, value);
            return this;
        }

        public PartnersBuilder WithActive(bool value)
        {
            _fixture = _fixture.With(x => x.IsActive, value);
            return this;
        }

        public PartnersBuilder WithNumberIssuedPromoCodes(int value)
        {
            _fixture = _fixture
                .With(x => x.NumberIssuedPromoCodes, value);

            return this;
        }

        public PartnersBuilder WithPartnerLimits(params PartnerPromoCodeLimit[] values)
        {
            _fixture = _fixture
                .With(x => x.PartnerLimits,
                    values.ToList() ??
                    new List<PartnerPromoCodeLimit>());

            return this;
        }

        public Partner Create() => _fixture.Create();
    }
}
