﻿using AutoFixture;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders
{
    public class PartnerPromoCodeLimitBuilder
    {
        private AutoFixture.Dsl.IPostprocessComposer<PartnerPromoCodeLimit> _fixture;

        public PartnerPromoCodeLimitBuilder()
        {
            _fixture = new Fixture()
                .Build<PartnerPromoCodeLimit>()
                .Without(x => x.Partner);
        }

        public PartnerPromoCodeLimitBuilder WithCancelDate(DateTime? value)
        {
            _fixture = _fixture.With(x => x.CancelDate, value);
            return this;
        }

        public PartnerPromoCodeLimit Create() => _fixture.Create();
    }
}