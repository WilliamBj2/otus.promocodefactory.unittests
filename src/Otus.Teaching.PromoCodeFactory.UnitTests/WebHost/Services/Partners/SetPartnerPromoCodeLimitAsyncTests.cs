﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.CustomExtensions.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Services.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersMockRepository;
        private readonly PartnersService _partnersService;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _partnersMockRepository = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersService = fixture
                .Build<PartnersService>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// Если партнер не найден => ошибка NotFoundException
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimit_PartnerIsNotFound_ReturnsNotFoundExceptionAsync()
        {
            // Arrange
            var partnerId = Guid.Parse("9c8d22fa-6232-4c73-9b62-ba61b81453f2");
            var partnerPromoCodeLimit =
                new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            Partner partner = null;

            _partnersMockRepository
                .Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner);

            // Act
            Func<Task> act = async () => await _partnersService
                .SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimit);

            // Assert
            await act.Should().ThrowExactlyAsync<NotFoundException>();
        }

        /// <summary>
        /// Если партнер заблокирован => ошибка ConflictException
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_PartnerIsNotActive_ReturnsConflictExceptionAsync()
        {
            // Arrange
            var partnerId = Guid.Parse("9c8d22fa-6232-4c73-9b62-ba61b81453f2");
            var partnerPromoCodeLimit =
                new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            var partner = new PartnersBuilder()
                .WithId(partnerId)
                .WithActive(false).Create();

            _partnersMockRepository
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            Func<Task> act = async () => await _partnersService
                .SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimit);

            // Assert
            await act.Should().ThrowExactlyAsync<ConflictException>();
        }

        /// <summary>
        /// Если на партнере установлен лимит => обнуляем количество промокодов (NumberIssuedPromoCodes)
        /// Также, если на партнере установлен лимит => отключаем предыдущий лимит
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_IfSetLimit()
        {
            // Arrange
            var partnerId = Guid.Parse("9c8d22fa-6232-4c73-9b62-ba61b81453f2");
            var partnerPromoCodeLimit =
                new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            var partnerLimit =
                new PartnerPromoCodeLimitBuilder()
                    .WithCancelDate(null)
                    .Create();

            var partner = new PartnersBuilder()
                .WithId(partnerId)
                .WithNumberIssuedPromoCodes(10)
                .WithActive(true)
                .WithPartnerLimits(partnerLimit).Create();

            _partnersMockRepository
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersService
                .SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimit);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
            partnerLimit.CancelDate.Should().NotBeNull();
        }

        /// <summary>
        /// Если лимит отсутствует => количество промокодов остается без изменений
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_IfNoLimit_IssuedPromoCodesAsyncIsNotZero()
        {
            // Arrange
            var partnerId = Guid.Parse("9c8d22fa-6232-4c73-9b62-ba61b81453f2");
            var partnerPromoCodeLimit =
                new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            var partnerLimit =
                new PartnerPromoCodeLimitBuilder()
                    .Create();

            var partner = new PartnersBuilder()
                .WithId(partnerId)
                .WithNumberIssuedPromoCodes(10)
                .WithActive(true)
                .WithPartnerLimits(partnerLimit).Create();

            _partnersMockRepository
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersService
                .SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimit);

            // Assert
            partner.NumberIssuedPromoCodes.Should().NotBe(0);
        }

        /// <summary>
        /// Если лимит меньше или равен 0 => ошибка ConflictException
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_LimitIsZero_ReturnsBadRequestAsync()
        {
            // Arrange
            var partnerId = Guid.Parse("9c8d22fa-6232-4c73-9b62-ba61b81453f2");
            var partnerPromoCodeLimit = new Fixture()
                .Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, 0)
                .Create();

            var partner = new PartnersBuilder()
                .WithId(partnerId)
                .WithActive(true)
                .WithPartnerLimits().Create();

            _partnersMockRepository
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            Func<Task> act = async () => await _partnersService
                .SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimit);

            // Assert
            await act.Should().ThrowExactlyAsync<ConflictException>();
        }

        /// <summary>
        /// Убедиться, что сохранили новый лимит в БД
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_AddNewLimit_NewLimitAsync()
        {
            // Arrange
            var partnerId = Guid.Parse("9c8d22fa-6232-4c73-9b62-ba61b81453f2");
            var partnerPromoCodeLimit = new Fixture()
                .Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, 10)
                .Create();

            var partner = new PartnersBuilder()
                .WithId(partnerId)
                .WithActive(true)
                .WithPartnerLimits().Create();

            _partnersMockRepository
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersService
                .SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimit);

            // Assert
            _partnersMockRepository.Verify(repo => repo.UpdateAsync(partner), times: Times.Once);
        }
    }
}