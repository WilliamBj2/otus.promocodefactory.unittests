﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.CustomExtensions.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Abstractions;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class CancelPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IPartnersService> _partnersService;
        private readonly PartnersController _partnersController;

        public CancelPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _partnersService = fixture.Freeze<Mock<IPartnersService>>();
            _partnersController = fixture
                .Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// Если в сервисе ловим исключение NotFoundException => результат NotFound
        /// </summary>
        [Fact]
        public async void CancelPartnerPromoCodeLimit_NotFoundException_ReturnsNotFoundAsync()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            _partnersService
                .Setup(srv => srv.CancelPartnerPromoCodeLimitAsync(It.IsAny<Guid>()))
                .ThrowsAsync(new NotFoundException());

            // Act
            var result = await _partnersController.CancelPartnerPromoCodeLimitAsync(partnerId);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если в сервисе ловим любое другое исключение помимо NotFoundException => результат BadRequest
        /// </summary>
        [Fact]
        public async void CancelPartnerPromoCodeLimit_OtherNoNotFoundException_ReturnsBadRequestAsync()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            _partnersService
                .Setup(srv => srv.CancelPartnerPromoCodeLimitAsync(It.IsAny<Guid>()))
                .ThrowsAsync(new ConflictException());

            // Act
            var result = await _partnersController.CancelPartnerPromoCodeLimitAsync(partnerId);

            // Assert
            result.Should().BeAssignableTo<BadRequestResult>();
        }

        /// <summary>
        /// Если в сервисе обошлись без исключений => результат NoContent
        /// </summary>
        [Fact]
        public async void CancelPartnerPromoCodeLimit_NoException_ReturnNoContentAsync()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            _partnersService
                .Setup(srv => srv.CancelPartnerPromoCodeLimitAsync(It.IsAny<Guid>()))
                .Returns(Task.CompletedTask);

            // Act
            var result = await _partnersController.CancelPartnerPromoCodeLimitAsync(partnerId);

            // Assert
            result.Should().BeAssignableTo<NoContentResult>();
        }
    }
}