﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.CustomExtensions.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Abstractions;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IPartnersService> _partnersService;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _partnersService = fixture.Freeze<Mock<IPartnersService>>();
            _partnersController = fixture
                .Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// Если в сервисе ловим исключение NotFoundException => результат NotFound
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimit_NotFoundException_ReturnsNotFoundAsync()
        {
            // Arrange
            var partnerId = Guid.Parse("9c8d22fa-6232-4c73-9b62-ba61b81453f2");
            var partnerPromoCodeLimit =
                new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            _partnersService
                .Setup(srv => srv.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), partnerPromoCodeLimit))
                .ThrowsAsync(new NotFoundException());

            // Act
            var result = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimit);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если в сервисе ловим любое другое исключение помимо NotFoundException => результат BadRequest
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_OtherNoNotFoundException_ReturnsBadRequestAsync()
        {
            // Arrange
            var partnerId = Guid.Parse("9c8d22fa-6232-4c73-9b62-ba61b81453f2");
            var partnerPromoCodeLimit =
                new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            _partnersService
                .Setup(srv => srv.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), partnerPromoCodeLimit))
                .ThrowsAsync(new ConflictException());

            // Act
            var result = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimit);

            // Assert
            result.Should().BeAssignableTo<BadRequestResult>();
        }

        /// <summary>
        /// Если в сервисе обошлись без исключений => результат CreatedAtAction
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimit_NoException_ReturnPartnersNewLimitIdsPairAsync()
        {
            // Arrange
            var partnerId = Guid.Parse("9c8d22fa-6232-4c73-9b62-ba61b81453f2");
            var partnerPromoCodeLimit =
                new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            _partnersService
                .Setup(srv => srv.SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimit))
                .ReturnsAsync((partnerId, It.IsAny<Guid>()));

            // Act
            var result = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimit);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }
    }
}